﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class EmployeeManagement
    {
        //Employee employee =  new Employee();
        Employee[] listEmployee = new Employee[100];
        public Employee AddEmployee()
        {
            Employee newEmployee = new Employee();
            Console.Write("Nhập tên Nhân viên: ");
            newEmployee.Name = Console.ReadLine();
            while (true)
            {
                try
                {
                    Console.Write("Nhập tuổi Nhân viên: ");
                    newEmployee.Age = int.Parse(Console.ReadLine());
                    if (newEmployee.Age <= 0)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Dữ liệu nhập vào phải là kiểu số lớn hơn 0 !");
                    continue;
                }
                break;
            }
            while (true)
            {
                try
                {
                    Console.Write("Nhập lương Nhân viên: ");
                    newEmployee.Salary = float.Parse(Console.ReadLine());
                    if (newEmployee.Salary <= 0)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Dữ liệu nhập vào phải là kiểu số lớn hơn 0 !");
                    continue;
                }
                break;
            }
            Console.Write("Nhập chức vụ Nhân viên: ");
            newEmployee.Position = Console.ReadLine();

            for (int i = 0; i < listEmployee.Length; i++)
            {
                if (listEmployee[i] == null)
                {
                    listEmployee[i] = newEmployee;
                    break;
                }
            }
            Console.WriteLine("Đã thêm Nhân viên mới vào danh sách !");
            return newEmployee;
        }

        public void GetEmpolyees()
        {
            if (listEmployee[0] == null)
            {
                Console.WriteLine("Danh sách Nhân viên trống !");
            }
            else
            {
                Console.WriteLine("Danh sách Nhân viên :");
                for (int i = 0; i < listEmployee.Length; i++)
                {
                    if (listEmployee[i] == null)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("\nNhân viên " + (i + 1) + " : ");
                        Console.WriteLine("Tên Nhân viên : " + listEmployee[i].Name);
                        Console.WriteLine("Tuổi Nhân viên : " + listEmployee[i].Age);
                        Console.WriteLine("Vị trí công việc : " + listEmployee[i].Position);
                        Console.WriteLine("Lương Nhân viên : " + listEmployee[i].Salary);
                    }
                }
            }
        }

        public Employee GetEmployeeByName(string name)
        {
            Employee existedEmployee = new Employee();
            Console.Write("Nhập tên Nhân viên: ");
            name = Console.ReadLine();
            for (int i = 0; i <= listEmployee.Length; i++)
            {
                if (listEmployee[i] == null)
                {
                    Console.WriteLine("Danh sách Nhân viên trống !");
                    break;
                }
                else if (listEmployee[i].Name.Equals(name))
                {
                    existedEmployee = listEmployee[i];
                    break;
                }
                else
                {
                    break;
                }
            }
            return existedEmployee;
        }

        public bool ConfirmDelete(string choice)
        {
            bool confirm = false;
            Console.WriteLine("Người dùng có chắc chắn muốn xóa Nhân viên này ?");
            Console.Write("Nhập lựa chọn (Yes/No): ");
            choice = Console.ReadLine();
            if (string.Equals(choice, "Yes", StringComparison.OrdinalIgnoreCase))
            {
                confirm = true;
            }
            else if (string.Equals(choice, "No", StringComparison.OrdinalIgnoreCase))
            {
                confirm = false;
            }
            else
            {
                Console.WriteLine("Lựa chọn không hợp lệ ! Vui lòng lựa chọn Yes/No !");
            }
            return confirm;
        }

        public void DeleteEmployee(string employeeName)
        {
            string choice = "";
            bool confirm = false;
            Employee existedEmployee = new Employee();
            existedEmployee = GetEmployeeByName(employeeName);
            if (existedEmployee.Name == null)
            {
                Console.WriteLine("Nhân viên không tồn tại !");
            }
            else
            {
                Console.WriteLine("Thông tin Nhân viên: ");
                Console.WriteLine("Tên Nhân viên: " + existedEmployee.Name);
                Console.WriteLine("Tên Nhân viên: " + existedEmployee.Age);
                Console.WriteLine("Tên Nhân viên: " + existedEmployee.Position);
                Console.WriteLine("Tên Nhân viên: " + existedEmployee.Salary);
                confirm = ConfirmDelete(choice);
                if (confirm == false)
                {
                    Console.WriteLine("Thực hiện xóa Nhân viên không thành công !");
                }
                else
                {
                    for (int i = 0; i < listEmployee.Length - 1; i++)
                    {
                        listEmployee[i] = listEmployee[i + 1];
                    }
                    Console.WriteLine("Đã xóa Nhân viên khỏi danh sách !");
                }
            }
        }

        public void UpdateEmployee(string employeeName)
        {
            Employee existedEmployee = new Employee();
            existedEmployee = GetEmployeeByName(employeeName);
            if (existedEmployee.Name == null)
            {
                Console.WriteLine("Nhân viên không tồn tại !");
            }
            else
            {
                Console.WriteLine("Thông tin Nhân viên: ");
                Console.WriteLine("Tên Nhân viên: " + existedEmployee.Name);
                Console.WriteLine("Tuổi Nhân viên: " + existedEmployee.Age);
                Console.WriteLine("Chức vụ Nhân viên: " + existedEmployee.Position);
                Console.WriteLine("Lương Nhân viên: " + existedEmployee.Salary);
                Console.WriteLine("\nCập nhật thông tin Nhân viên: ");
                Employee updatedEmployee = new Employee();
                Console.Write("Nhập tên Nhân viên: ");
                updatedEmployee.Name = Console.ReadLine();
                //string age = "";
                while (true)
                {
                    try
                    {
                        Console.Write("Nhập tuổi Nhân viên: ");
                        updatedEmployee.Age = int.Parse(Console.ReadLine());
                        if(updatedEmployee.Age <= 0)
                        {
                            throw new Exception();
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Vui lòng nhập kiểu số lớn hơn 0 !");
                        continue;
                    }
                    break;
                }
                Console.Write("Nhập vị trí công việc: ");
                updatedEmployee.Position = Console.ReadLine();
                while (true)
                {
                    try
                    {
                        Console.Write("Nhập lương Nhân viên: ");
                        updatedEmployee.Salary = float.Parse(Console.ReadLine());
                        if (updatedEmployee.Salary <= 0)
                        {
                            throw new Exception("Vui lòng nhập kiểu số lớn hơn 0 !") ;
                        }
                    }
                    catch(Exception)
                    {
                        continue;
                    }
                    break;
                }
                if (updatedEmployee.Name == "")
                {
                    updatedEmployee.Name = existedEmployee.Name;
                }
                /*if (age == "")
                {
                    updatedEmployee.Age = existedEmployee.Age;
                }
                else
                {
                    updatedEmployee.Age = Convert.ToInt32(age);
                }*/
                if (updatedEmployee.Position == "")
                {
                    updatedEmployee.Position = existedEmployee.Position;
                }
                /*if (salary == "")
                {
                    updatedEmployee.Salary = existedEmployee.Salary;
                }
                else
                {
                    updatedEmployee.Salary = float.Parse(salary);
                }*/
                existedEmployee.Name = updatedEmployee.Name;
                existedEmployee.Age = updatedEmployee.Age;
                existedEmployee.Position = updatedEmployee.Position;
                existedEmployee.Salary = updatedEmployee.Salary;
                Console.WriteLine("Cập nhật thông tin Nhân viên thành công !");
                Console.ReadKey();
                Console.Clear();               
                Console.WriteLine("Thông tin Nhân viên: ");
                Console.WriteLine("Tên Nhân viên: " + existedEmployee.Name);
                Console.WriteLine("Tuổi Nhân viên: " + existedEmployee.Age);
                Console.WriteLine("Chức vụ Nhân viên: " + existedEmployee.Position);
                Console.WriteLine("Lương Nhân viên: " + existedEmployee.Salary);
            }
        }
    }
}
