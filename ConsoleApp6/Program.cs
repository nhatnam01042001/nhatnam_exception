﻿// See https://aka.ms/new-console-template for more information
using ConsoleApp6;
using System;
using System.Net.Cache;
//using NhatNam_Class.EmployeeManagement;
Console.OutputEncoding = System.Text.Encoding.UTF8;
Employee employee = new Employee();
//Employee[] listEmployee = new Employee[100];
EmployeeManagement _employee = new EmployeeManagement();
string employeeName = "";
int userChoice = 0;
int choice = 0;
do
{
    Console.WriteLine("Hệ thống quản lí Nhân viên");
    Menu();
    while (true)
    {
        try
        {
            Console.Write("Người dùng nhập lựa chọn : ");
            choice = int.Parse(Console.ReadLine());
            //userChoice = int.Parse(choice);
        }
        catch (Exception)
        {
            Console.WriteLine("Vui lòng nhập kiểu dữ liệu số nguyên !");
            continue;
        }
        break;
    }
    switch (choice)
    {
        case 1:
            _employee.GetEmpolyees();
            break;
        case 2:
            _employee.AddEmployee();
            break;
        case 3:
            _employee.DeleteEmployee(employeeName);
            break;
        case 4:
            _employee.UpdateEmployee(employeeName);
            break;
        case 5:
            Console.WriteLine("Bạn đã thoát khỏi hệ thống !");
            break;
        default:
            Console.WriteLine("Lựa chọn không hợp lệ ! Nhập lựa chọn (1-5) !");
            break;
    }
    Console.ReadKey();
    Console.Clear();
} while (userChoice != 5);
void Menu()
{
    Console.WriteLine("1.Xem danh sách Nhân viên");
    Console.WriteLine("2.Thêm Nhân viên");
    Console.WriteLine("3.Xóa Nhân viên");
    Console.WriteLine("4.Cập nhật thông tin Nhân viên");
    Console.WriteLine("5.Thoát");
}
