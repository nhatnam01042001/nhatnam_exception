﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    public class Employee
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Position { get; set; }
        public float Salary { get; set; }
        public Employee()
        {
            this.Name = Name;
            this.Age = Age;
            this.Position = Position;
            this.Salary = Salary;
        }
        public Employee(string name, int age, string postion, float salary)
        {
            this.Name = name;
            this.Age = age;
            this.Position = postion;
            this.Salary = salary;
        }
    }
}
